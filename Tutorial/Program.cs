﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace HackerRankTutorial
{
    class Program
    {
        static void Main(string[] args)
        {
            int valToGet;
            int arrLength;

            int.TryParse(Console.ReadLine(), out valToGet);
            int.TryParse(Console.ReadLine(), out arrLength);

            var inputArray = Console.ReadLine();

            var array = new int[arrLength];

            if (inputArray == null) return;

            var arrayChar = inputArray.Split(' ');

            for (var i = 0; i < arrLength; i++)
            {
                int element;
                int.TryParse(arrayChar[i].ToString(), out element);
                array[i] = element;
            }

            var marker = arrLength - 1; // / 2;

            while (array[marker] != valToGet && marker != array[0])
            {
                var midpoint = marker / 2;

                var midpointVal = array[midpoint];

                if (array[marker] == valToGet) break;

                marker = valToGet < midpointVal ? midpoint : marker - 1;
            }

            if (valToGet != array[marker])
                Console.WriteLine("Not found");
            else
                Console.WriteLine(marker);
        }
    }
}
